#!/usr/bin/env bash

# Copyright © 2023 The Authors (See AUTHORS file)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e

# Platform specific handling - Defaults for Linux
DOCKER_ISACTIVE="systemctl is-active docker > /dev/null"
SEDREGEX="-r"
if [ "$(uname)" == "Darwin" ]; then
    # Do something under Mac OS X platform   
	DOCKER_ISACTIVE="docker stats --no-stream > /dev/null 2>&1"
	SEDREGEX="-E"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # Do something under 32 bits Windows NT platform
	echo "Win32 Not supported"
	exit 1
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    # Do something under 64 bits Windows NT platform
	echo "Win64 Not supported"
	exit 1
fi

# Generating specific configuration that must not be saved in the repository (passwords)
DIRECTORY=$(dirname $0)
if [ ! -f $DIRECTORY/docker-compose.override.yml ]; then
	echo Creating default docker-compose.override.yml
	PASSWORD=$(sed -n $SEDREGEX 's/.*MONGO_INITDB_ROOT_PASSWORD: *([a-zA-Z0-9]+).*/\1/p' $DIRECTORY/../dev-common/docker-compose.override.yml)
	sed "s/<PASSWORD>/$PASSWORD/g" < $DIRECTORY/docker-compose.overridetemplate.yml > $DIRECTORY/docker-compose.override.yml
fi

if eval $DOCKER_ISACTIVE; then
	echo ""
	echo "Start services in project?"
	while true; do
		echo "Press Ctrl-C to abort, [Yes]/No to proceed"
		read yn
		yn=${yn:-yes}
		case $yn in 
			[yY][eE][sS]|[yY] ) docker-compose up -d;
					break;;
			[nN][oO]|[nN] ) echo "Skipping. Not starting services";
					break;;
			* ) echo "Invalid response: '$yn'";;
		esac
	done
else
	echo "Docker not running. You probably need to start up dev-common"
fi

echo ""
echo "All done"
