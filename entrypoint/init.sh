#!/bin/sh

set -e

DIR=/app/public
# config.js is located in /app/public because it is loaded directly in index.html

echo "Generating config.js as $(whoami) in $DIR"
envsubst < $DIR/config.tmpl.js | tee $DIR/config.js

echo "Executing CMD as $(whoami):" $@
$@
