/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import type { Directive } from 'vue';

export const RegexpInputDirective: Directive = {
    mounted(el, binding, vnode) {
        const pattern = binding.value || '';
        const regex = new RegExp(`^${pattern}$`);

        el.addEventListener('keydown', (event: KeyboardEvent) => {
            const allowedKeys = ['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown', 'Home', 'End', 'Delete', 'Backspace'];
            if (allowedKeys.includes(event.key) || event.ctrlKey || event.metaKey || event.altKey) {
                return;
            }

            const inputElement = event.target as HTMLInputElement;
            const currentValue = inputElement.value;
            const selectionStart = inputElement.selectionStart || 0;
            const selectionEnd = inputElement.selectionEnd || 0;
            const nextValue = currentValue.slice(0, selectionStart) + event.key + currentValue.slice(selectionEnd);

            if (!regex.test(nextValue)) {
                console.log(`RegexpInputDirective: ${nextValue} not allowed`)
                event.preventDefault();
            }
        });
    },
};