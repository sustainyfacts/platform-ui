/**
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { defineStore } from 'pinia'

export class User {
  name !: string
}

class Tokens {
  access_token !: string
  id_token !: string
}

class Auth {
  user!:User
}

const authKey = "user"

function arrayBufferToCodeChallenge(buffer: ArrayBuffer): string {
	var binary = '';
	var bytes = new Uint8Array(buffer);
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return window.btoa(binary)
		.replace(/\+/g, "-")
		.replace(/\//g, "_")
		.replace(/=/g, "")
}

export const useAuthStore = defineStore(authKey, {
  state: () => {
    var auth = undefined as Auth | undefined
    var authString = localStorage.getItem(authKey)
    if (authString != null) {
        auth = JSON.parse(authString)
    }
    return {
      session : auth
    }
  },
  actions: {
    async login(returnPath: string) {
      if (returnPath == "/logout") {
        returnPath = "/"
      }
      sessionStorage.setItem("returnPath", returnPath)
			var redirectURL = encodeURIComponent(window.Config.UI_URL + "/login/callback")
			var clientId = encodeURIComponent(window.Config.AUTH_CLIENT_ID)

			// Generate random string
			const codeVerifier = window.crypto.getRandomValues(new Uint32Array(8)).join('')

			sessionStorage.setItem("codeVerifier", codeVerifier)
			const encoder = new TextEncoder();
			const data = encoder.encode(codeVerifier);
			const codeChallengeAB = await window.crypto.subtle.digest("SHA-256", data);
			const codeChallenge = arrayBufferToCodeChallenge(codeChallengeAB)
			const authURI = window.Config.AUTH_SERVER_URL + `/oauth/v2/authorize?client_id=${clientId}&redirect_uri=${redirectURL}&code_challenge=${codeChallenge}&response_type=code&scope=openid%20email%20profile%20urn%3Azitadel%3Aiam%3Aorg%3Aproject%3Aid%3Azitadel%3Aaud&code_challenge_method=S256`
			
			// Redirect user
			window.location.href = authURI
    },
    logout() {
      localStorage.removeItem(authKey)
      this.session = undefined
      fetch(window.Config.API_URL + "/logout",{credentials:"include"})
    },
    async fetchToken(code: string) {
      var codeVerifier = sessionStorage.getItem("codeVerifier")
      if (codeVerifier == null) {
        console.error("codeVerifier not found - Authentication failed")
        return
      }
      sessionStorage.removeItem("codeVerifier")

      var request: Map<string, string> = new Map([
        ['grant_type', 'authorization_code'],
        ['client_id', window.Config.AUTH_CLIENT_ID],
        ['code_verifier', codeVerifier],
        ['redirect_uri', window.Config.UI_URL + "/login/callback"],
        ['code', code],
      ])

      var details: string[] = [];
      request.forEach((value, key) => {
        var encodedKey = encodeURIComponent(key);
        var encodedValue = encodeURIComponent(value);
        details.push(encodedKey + "=" + encodedValue);
      })
      var body = details.join("&")

      const tokenResponse = await fetch(window.Config.AUTH_SERVER_URL + "/oauth/v2/token",
        {
          method: "POST",
          body: body,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });

      var tokens = await tokenResponse.json() as Tokens;
      console.log(`Tokens: ${tokens}`)

      const loginResponse = await fetch(window.Config.API_URL + "/login",
        {
          method: "POST",
          body: JSON.stringify(tokens),
          credentials: "include",
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${tokens.access_token}`
          }
        });
      
      this.session = new Auth()
      this.session.user = await loginResponse.json() as User;

      localStorage.setItem(authKey, JSON.stringify(this.session))
    },
  },
});