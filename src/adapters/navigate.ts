/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { createRouter, createWebHistory } from 'vue-router'
import Welcome from '@/views/Welcome.vue'
import Test from '@/views/Test.vue'
import Company from '@/views/CompanyView.vue'
import CompanyDetails from '@/views/CompanyDetails.vue'
import DisclosureDetails from '@/views/DisclosureDetails.vue'
import CompanyDisclosures from '@/views/CompanyDisclosures.vue'
import CompanyFacts from '@/views/CompanyFacts.vue'
import CompanyRatings from '@/views/CompanyRatings.vue'
import Admin from '@/views/Admin.vue'
import AdminDataModels from '@/views/AdminDataModels.vue'
import AdminRatingModels from '@/views/AdminRatingModels.vue'
import Empty from '@/views/Empty.vue'
import { useAuthStore } from '@/adapters/stores'
import Logout from '@/views/Logout.vue'

export const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            component: Welcome
        },
        {
            path: '/login/callback',
            name: 'LoginCallback',
            component: Empty,
            beforeEnter: async (to, from) => {
                var params = new URL(window.location.href).searchParams
                var code = params.get("code")
                if (code != null) {
                    var auth = useAuthStore()
                    await auth.fetchToken(code)
                }
                return sessionStorage.getItem("returnPath") || '/'
            },
        },
        {
            path: '/login',
            component: Empty,
            beforeEnter: (to, from) => {
                var auth = useAuthStore()
                auth.login(from.path)
            },
        },
        {
            path: '/logout',
            component: Logout,
            beforeEnter: (to, from) => {
                var auth = useAuthStore()
                auth.logout()
            },
        },
        {
            path: '/test',
            component: Test
        },
        {
            path: '/company/:companyref/',
            component: Company,
            children: [
                {
                    path: '',
                    component: CompanyDetails,
                },
                {
                    path: 'disclosures',
                    component: CompanyDisclosures,
                },
                {
                    path: 'disclosures/:disclosureid',
                    component: DisclosureDetails,
                },
                {
                    path: 'facts',
                    component: CompanyFacts,
                },
                {
                    path: 'rating',
                    component: CompanyRatings,
                },
            ]
        },
        {
            path: '/admin/',
            component: Admin,
            children: [
                {
                    path: 'datamodels',
                    component: AdminDataModels,
                },
                {
                    path: 'ratingmodels',
                    component: AdminRatingModels,
                },
                {
                    path: 'other',
                    component: Empty,
                }
            ]
        }
    ]
})

export class Link {
    private path: string
    constructor(path: string) {
        this.path = path
    }
    go() {
        const newPath = this.path.replace("${companyRef}", companyRef)
        router.push(newPath)
    }
}

// Last visited company
var companyRef = ""

class Navigator {
    setCurrentCompany(ref: string) {
        companyRef = ref
    }
    company_current() {
        // We defer to the path evaluation when using go()
        // Which mean we can build the link at the component setup
        // and navigate to the company loaded later on
        return new Link("/company/${companyRef}/")
    }
    company(ref: string) {
        companyRef = ref
        return new Link("/company/${companyRef}/")
    }
    company_create() {
        companyRef = ""
        return new Link("/company/create/")
    }
    company_disclosures() {
        return new Link("/company/${companyRef}/disclosures")
    }
    company_facts() {
        return new Link("/company/${companyRef}/facts")
    }
    company_rating() {
        return new Link("/company/${companyRef}/rating")
    }
    company_disclosure(disclosureId: string) {
        return new Link("/company/${companyRef}/disclosures/" + disclosureId)
    }
    company_disclosure_create() {
        return new Link("/company/${companyRef}/disclosures/create")
    }
    home() {
        return new Link("/")
    }
    logout = new Link("/logout")
    admin = new Link("/admin/")
    admin_data() {
        return new Link("/admin/datamodels")
    }
    admin_rating() {
        return new Link("/admin/ratingmodels")
    }
    admin_other() {
        return new Link("/admin/other")
    }
}

export const pages = [
    { name: 'Work In Progress ApS', href: '#', current: false },
    { name: 'Details', href: '#', current: true },
  ]

export const nav = new Navigator()