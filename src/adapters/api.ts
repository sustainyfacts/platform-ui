/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { useAuthStore } from '@/adapters/stores'

const baseURL = window.Config.API_URL

type successCallback = (json: any) => void;
type errorCallback = (error: APIError) => void;

export class APIError extends Error {
  details!: string

  constructor(message: string, details?: string) {
    super(message);
    if (details != null) {
      this.details = details
    }
  }
}

class APIAdapter {

  // Get the full URL from a path
  url(path: string) {
    return baseURL + path
  }

  // Method to make simple GET requests with a call back
  get(path: string, successCallback: successCallback, errorCallback?: errorCallback) {
    // Build the query URL
    const url = baseURL + path
    fetch(url, { credentials: "include" })
      .then(getJSONHandler(successCallback))
      .catch(getErrorHandler(errorCallback))
  }

  // Method to make simple PATCH requests with JSON body and a call back
  patch(path: string, json: any, successCallback: successCallback, errorCallback?: errorCallback) {
    // Build the query URL
    const url = baseURL + path
    const body = JSON.stringify(json)
    fetch(url,
      {
        method: "PATCH", credentials: 'include', headers: {
          "Content-Type": "application/json"
        }, body: body
      })
      .then(getJSONHandler(successCallback))
      .catch(getErrorHandler(errorCallback))
  }

  // Method to make simple POST requests with JSON body and a call back
  post(path: string, json: any, successCallback: successCallback, errorCallback?: errorCallback) {
    const body = JSON.stringify(json)
    this.postContent(path, body, "application/json", successCallback, errorCallback)
  }

  // Method to make simple POST requests with text body of a given content type and callbacks
  postContent(path: string, body: string, contentType: string, successCallback: successCallback, errorCallback?: errorCallback) {
    // Build the query URL
    const url = baseURL + path
    fetch(url,
      {
        method: "POST", credentials: "include",
        headers: { "Content-Type": contentType }, body: body
      })
      .then(getJSONHandler(successCallback))
      .catch(getErrorHandler(errorCallback))
  }

  // Method to make simple PUT requests with text body of a given content type and callbacks
  putContent(path: string, body: string, contentType: string, successCallback: successCallback, errorCallback?: errorCallback) {
    // Build the query URL
    const url = baseURL + path
    fetch(url,
      {
        method: "PUT", credentials: "include",
        headers: { "Content-Type": contentType }, body: body
      })
      .then(getJSONHandler(successCallback))
      .catch(getErrorHandler(errorCallback))
  }
}

function getJSONHandler(successCallback: successCallback): (response: Response) => void {
  return async (response) => {
    const isJson = response.headers.get('content-type')?.includes('application/json');
    const data = isJson ? await response.json() : null;

    // check for error response
    if (!response.ok) {
      // Authentication error, login
      if (response.status == 401) {
        useAuthStore().login(window.location.pathname)
      }
      // get error message from body or default to response status
      const error = new APIError(data.status || response.statusText, data.error)
      return Promise.reject(error);
    }

    successCallback(data)
  }
}
function getErrorHandler(errorCallback?: errorCallback): (error: APIError) => void {
  return (error: APIError) => {
    console.error(JSON.stringify(error))
    if (errorCallback != null) {
      errorCallback(error)
    }
  }
}
export const api = new APIAdapter()