/**
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import { router } from '@/adapters/navigate'
import { RegexpInputDirective } from '@/adapters/input'
import { useAuthStore } from '@/adapters/stores'

import './assets/main.css'

// Add Window to inject configuration into app
declare global {
	interface Window {
		Config: Record<string, any>;
	}
}

const app = createApp(App)
const pinia = createPinia()

app.use(pinia)
app.use(router)
app.directive('regexp-input', RegexpInputDirective)

app.mount('#app')

router.beforeEach((to, from, next) => {
	if (to.name == 'LoginCallback') {
		next()
	} else {
		const auth = useAuthStore()

		if (!auth.session) {
			auth.login(to.path)
		}
		next()
	}
})