/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { ref } from 'vue'

export class Company {
  id !: string
  name !: string
  country !: string
  nationalid !: string
  website !: string
  about !: string
  brands !: string[]

  getRef(): string {
    return this.country + "-" + this.nationalid
  }
}

export class Facts {
  id !: string
  company_id !: string
  currency!: string
  from_year !: number
  to_year !: string
  Data !: any
}

export const loadedCompany = ref({} as Company)
