/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { api, APIError } from '@/adapters/api'

export interface Property {
  id: string
  public_id?: string
  name: string
  description: string
  type: string
  unit?: string
  values?: string
  properties?: Property[]
}

export interface DataModel {
  id: string
  public_id: string
  name: string
  description: string
  version: string
  disclosures: Property[]
}

var dataModels: DataModel[]

export async function loadDataModel(): Promise<DataModel[]> {
  if (dataModels != undefined) {
    return new Promise((resolve) => resolve(dataModels))
  }
  return new Promise((resolve) => {
    api.get('/datamodels?detailled=true', (data: any) => {
      dataModels = data
      resolve(dataModels)
    })
  })
}