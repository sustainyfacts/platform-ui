/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { reactive } from 'vue'

class Notification {
  id: number
  text: string
  details: string
  status: 'Error' | 'Success' | 'Warning'
  visible: boolean
  constructor(id: number, text: string, details: string, status: 'Error' | 'Success' | 'Warning') {
    this.id = id
    this.text = text
    this.details = details
    this.status = status
    this.visible = true
  }
  public isError() {
    return this.status == 'Error'
  }
  public isSuccess() {
    return this.status == 'Success'
  }
  public hide() {
    this.visible = false
    instance.removeInvisible() 
  }
}

class Notifier {
  private notifications: Notification[] = []
  private counter = 0

  public success(text: string) {
    const n = new Notification(this.counter++, text, "", 'Success')
    this.notifications.push(n)
    // Hide after 3s
    setTimeout(() => n.hide(), 3000)
  }

  public error(text: string, details: string) {
    this.notifications.push(new Notification(this.counter++, text, details, 'Error'))
  }

  public getAll(): Notification[] {
    return this.notifications.filter((n) => n.visible)
  }

  public removeInvisible() {
    this.notifications = this.notifications.filter((n) => n.visible)
  }
}

const instance = reactive(new Notifier())

export default instance

