/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./src/**/*.{html,vue,js}",
    "./index.html"
  ],
  theme: {
    extend: {},
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      red: colors.red,
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      green: colors.emerald,
      yellow: colors.yellow,
      sf: {
        brown: '#b68679',
        DEFAULT: '#d4350b',
        dark: '#7a2008',
        lgray: '#f3f3f3',
        mgray: '#d9d9d9',
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
