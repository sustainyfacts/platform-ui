window.Config = {
	ENV: "dev",
	API_URL: "http://data-api.sustainyfacts.localhost",
	UI_URL: "http://ui.sustainyfacts.localhost",
	DEV_SERVER_HOST: "ui.sustainyfacts.localhost",
	DEV_SERVER_PORT: "80",
	AUTH_SERVER_URL: "https://sustainyfacts-8svmnf.zitadel.cloud",
	AUTH_CLIENT_ID: "233925354887166308@sustainyfacts",
}
