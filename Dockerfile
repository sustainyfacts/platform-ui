##########################################
#
# Base build
#
FROM node:19-alpine as build

# Bash and vim for easier debugging on live container
# Gettext for envsubst used in entrypoint
# Python3, Make and G++ for NPM to work
RUN apk add --no-cache --update \
	bash \
	vim \
	gettext \
	python3 \
	make \
	g++ \
	&& rm -rf /tmp/* /var/cache/apk/*
WORKDIR /build
COPY package*.json ./
RUN npm install
COPY . .

##########################################
#
# Development environment
# Targetted from Docker Compose
#
FROM build as dev
WORKDIR /app
RUN chown node:node .
COPY --from=build --chown=node:node /build/ ./
USER node:node
EXPOSE 80
ENTRYPOINT [ "/app/entrypoint/init.sh" ]
CMD ["npm", "run", "dev", "--", "--port 80", "--host 0.0.0.0"]

##########################################
#
# Production build
#
FROM build as prod-build
WORKDIR /app
RUN chown node:node .
COPY --from=build --chown=node:node /build/ ./
USER node:node
RUN npm run build

##########################################
#
# Production serve
#
FROM caddy:alpine
RUN apk add --no-cache --update \
	gettext \
	&& rm -rf /tmp/* /var/cache/apk/*
WORKDIR /app
COPY --from=prod-build /app/dist ./
COPY Caddyfile .
COPY /entrypoint ./entrypoint
EXPOSE 80
ENTRYPOINT [ "./entrypoint/init.sh" ]
CMD [ "caddy", "run" ]
